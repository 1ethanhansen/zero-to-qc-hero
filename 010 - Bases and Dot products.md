Summary:
- Basis of a [[009 - Linear Independence#^8308d5|subspace]] is a set of vectors that is [[009 - Linear Independence|linearly independent]] and [[008 - Linear Combinations#^51f3ae|spans]] the subspace ^fb1f98
- Dot Product: $$ \begin{bmatrix}
a_1 \\
a_2 \\
... \\
a_n
\end{bmatrix} \cdot \begin{bmatrix}
b_1 \\
b_2 \\
... \\
b_n
\end{bmatrix} = a_1b_1 + a_2b_2 + ... + a_nb_n $$ ^39f0f7
- Length of a vector: $$ ||\vec{a}|| = \sqrt{a_1^2 + a_2^2 + ... + a_n^2} $$
- $$ \vec{v} \cdot \vec{w} = \vec{w} \cdot \vec{v} $$
- $$ (\vec{v} + \vec{w}) \cdot \vec{x} = \vec{x} \cdot \vec{v} + \vec{x} \cdot \vec{w} $$
- $$ (c \vec{v}) \cdot \vec{w} = c(\vec{v} \cdot \vec{w}) $$

Analogy: A basis is almost like axes on a chart. You only need 2 axes pointing in different directions to define a 2-d chart, 3 would be overkill. Similarly, you only need 2 vectors pointing in different directions to define a subspace