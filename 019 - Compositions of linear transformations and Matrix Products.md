Summary:
- Composition of two [[017 - Linear Transformations|linear transformations]] is still a linear transformation
- $$ AB = \begin{bmatrix}
A\vec{b_1} & A\vec{b_2} & ... & A\vec{b_n}
\end{bmatrix} $$
- $$ ((H \circ G) \circ F) = (H \circ (G \circ F)) $$

Analogy: