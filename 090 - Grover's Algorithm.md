Summary:
- Task: given N bits, find a 1 (given implicitly in a truth table of a boolean function/circuit `C: {0, 1}^n -> {0, 1}` st N=2^n)
- In the "black box query model" you can only run the C, you can't look at it/analyze it
- Rotate, compute, rotate, compute, rotate...
- 