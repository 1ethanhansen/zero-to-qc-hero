An observable A is a measurable physical property which has a numerical value, for example, spin or momentum or energy ^615a63

$$ A = \sum_k \lambda_k | e_k \rangle \langle e_k | = \sum_k \lambda_k P_k $$

Conversely, to any normal operator A we can associate a measurement defined by the eigenvectors of A, which form an orthonormal basis, and use the eigenvalues of A to label the outcomes of this measurement
Standard measurement also called Z-measurement because of this
