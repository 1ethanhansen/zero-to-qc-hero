Summary:
- quantum materials' behavior rooted in quantum world
	- reduced dimensionality (1/2d instead of 3)
	- long coherence time and easy coupling are often in conflict
- Quantum Error Correction
	- bit flip errors flip 0 -> 1 or 1 -> 0
	- sign flips (bit flips but side ways)
- [[046 - Qubits and Quantum States, Quantum Circuits, Measurements Part 2#^c2f6b4|Entanglement]]
	- Neither of the individual qubits has a definite state, but the whole multi-qubit register *does* have a well-defined state
- Superdense coding
	- ![[Pasted image 20220513125836.png]]
	- question: do you have to phyiscally transmit the yellow qubit?
- [[038 - How Quantum Teleportation Works|Teleportation]]
- Entanglement Swapping
	- ![[Pasted image 20220513134934.png]]

Analogy:
