Summary:
- [[056 - Quantum 101#^9cbe04|no-cloning]] says you can't take an arbitrary state psi and generate two qubits with state psi 
	- Can't do this: $$ | \psi \rangle \to [black box] \to | \psi \rangle \otimes | \psi \rangle $$
	- you can't learn the state of a qubit with one copy of that qubit
	- CNOT gate you might think works, but does not
- Cloning biased coins
	- can't do it
- [[038 - How Quantum Teleportation Works]] (the Professor O'Donnell version)
	- 

Analogy:
- Quantum state ~= one-time flippable coin