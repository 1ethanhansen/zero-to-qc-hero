All linear transformations of density operators that can be written in the Stinespring (or, equivalently, Kraus) form represent physically realisable operations, and we call them quantum channels.

