Summary:
- AND gates can't be done on a quantum computer because all operations must be [[028 - Linear Algebra Kata#^e1fe7d|unitary]] (which implies [[020 - Inverse Matrices|invertible]]/reversible) and if I just tell you the output of AND was 0, you don't know if the input was 0&0 or 0&1 or 1&0
- However, you *can* build super AND and OR gates with reversibility by adding in garbage/ancilla qubits
- Theorem: Any classical circuit C computing `F: {0, 1}^n -> {0, 1}^m` can be efficiently converted to a reversible (and hence quantum) circuit $$ QC: \{ 0, 1\}^{n+a} -> \{0, 1\}^{m+g} \ | (n+a = m+g) $$
- Uncomputing garbage
	- Given `F: {0, 1}^n -> {0, 1}`  computed reversibly
	- you have f(x) is your output bit
	- g1..2(x) is garbage bits
	- CNOT with f(x) as control onto new answer bit
	- reverse the whole computation

Analogy: