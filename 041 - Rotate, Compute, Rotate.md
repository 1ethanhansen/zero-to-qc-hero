Summary:
- [[034 - Quantum Mechanics Distilled 1|quantum mechanics]] is just probability with minus signs
- Probabilistic computing
	- Deterministic code/circuits + coin flip instruction
	- Is it more powerful than deterministic?
		- Yes, by definition (do something random -> a prob. computer is better)
		- Maybe not, when just computing functions
		- Maybe yes, if we can trade error for efficiency
			- deterministic: n^4 best for determining if prime (assuming extended riemann hypothesis)
			- probabilistic: n^2 steps
	- Probabilistic computing has a chance of getting it wrong/failure
- Strongly believed: Every problem in P probabilistically is also in P deterministically
- QC gives exponential speedup over probabilistic computing for factoring
- Strongly believed: QC does **not** give exponential speedup for NP-complete
- One extra power of [[031 - Quantum Computing for the Very Curious 1|quantum computers]]
	- Discrete fourier transform in logn steps
	- Catch is you only get one sample from the fourier transform for each measurement
	- 

Analogy: