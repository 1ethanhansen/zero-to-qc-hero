Mixed states: We have some weird states qubits can get into like if Alice and Bob each have 1 qubit of an [[059 - The CHSH Game#^2dc166|EPR pair]] and bob measures his. What state is Alice's qubit in?

If we have a state given as a list of probabiliies (like a python dictionary or JSON)
```JSON
{
"p1":"psi1",
"p2":"psi2",
...
"pm":"psim"
}
```
and we measure that qudit in an orthonormal basis |u1>...|ud>
then probability of readout being `i` is $$ \sum_{j=1}^m p_j |\langle u_i | \psi_j \rangle |^2 = \sum_{j=1}^m p_j \langle u_i | \psi_j \rangle\langle \psi_j | u_i \rangle $$
Push the sum (and thus everything depending on j) into the middle, you get $$ = \langle u_i | \cdot \rho \cdot | u_i \rangle $$
All outcome probabilities of any measurement only depend on rho
$$ \rho = \sum_{j=1}^m p_j | \psi_j \rangle \langle \psi_j | $$ is called the density matrix of the mixed state

Example: 50% prob |0> 50% |1> has density matrix $$ \rho = \frac{1}{2} \begin{bmatrix}
1 \\ 0 \end{bmatrix} \begin{bmatrix} 1 & 0 \end{bmatrix} + \frac{1}{2} \begin{bmatrix}
0 \\ 1 \end{bmatrix} \begin{bmatrix} 0 & 1 \end{bmatrix} = \frac{1}{2} \begin{bmatrix} 1 & 0 \\ 0 & 1 \end{bmatrix} $$
