![[059 - The CHSH Game]]

CHSH inequality - an upper bound on classical correlations

devices are pre-programmed such that A0 always gives the same output as B0 and B1 and A1 gives the same output as B0, but A1 and B1 give opposite outputs
that is $A_xB_y = (-1)^{x \cdot y}$

Aristotelian logic tells us this cannot be right 100% of the time, 25% of the time it should be wrong (in disagreement with the above statement)

restate problem as $S = A_0B_0 + A_0B_1 + A_1B_0 - A_1B_1$ should = 4
$$ = A_0(B_0 + B_1) + A_1(B_0-B_1) $$
$$ = \pm 2 $$
$$ | \langle S \rangle | \leq 2 $$
^ for classical case (this is Bell's inequality / CHSH inequality)

That sum _S_ can be as high as $2\sqrt{2}$ under the quantum regime