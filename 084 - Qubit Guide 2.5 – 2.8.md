Summary:
- You can contstruct a unitary gate that is the square root of NOT (that is, applying it twice gives a NOT gate)
- This can't be done classically, any standard model of computation/mathematics/logic says there is no operation that can be done with two identical gates giving the output not
- H, X, Y, Z, and S form the Clifford Gates
	- $$ \text{S-gate} = \begin{bmatrix}
1 & 0 \\
0 & i
\end{bmatrix} $$ ^5e04e2
- X, Y, and Z are [[032 - Quantum Computing for the Very Curious 2#^3e59fb|Pauli Matrices]] AKA Pauli Spin Matrices
- HXH = Z
- HZH = X
- HYH = -Y

Analogy: