Summary:
- Decoherence supresses quantum interference ^b4cb0e
- Quantum computation can be viewed as a complex multi-particle quantum interference involving many computational paths through a computing device. The art of quantum computation is to shape quantum interference, through a sequence of computational steps, enhancing probabilities of correct outputs and suppressing probabilities of the wrong ones.
- EXP contains BQP contains BPP (maybe) contains P

Analogy: