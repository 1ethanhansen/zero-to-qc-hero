entropy denoted H(p) and
$$ 0 \leq H(p) \leq log_2(d) $$
only 0 when p_i = 1 (only one outcome)
only log(d) when uniform distribution

Quantum Info def 1:
The von Neumann entropy of a mixed state rho (dxd) is $$ H(\rho) = \sum_{i=1}^d \lambda_i log_2(\frac{1}{\lambda_i}) $$
(rho has eigen values lambda1..lambdad)

pure states always have entropy 0

$$ H(\rho_A) = H(\rho_B) $$ is numerical value quantifying how much entanglement

End result: you can only pack and send at most b bits of classical info into a b-qubit state