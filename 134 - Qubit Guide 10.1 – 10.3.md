quantum computation is often Fourier + Function + Fourier ([[041 - Rotate, Compute, Rotate]])

All operations must be reversible because they are unitary so computation is done in a reversible fashion as
$$ |x \rangle | y \rangle \to | x \rangle | y \oplus f(x) \rangle $$
$$ |x \rangle | 0 \rangle \to | x \rangle | f(x) \rangle $$
(look familiar? [[036 - How the Quantum Search Algorithm works 1|it should!]])

Revisiting [[106 - Qubit Guide 5.5 – 5.8#^529695|phase kick-back]]:
