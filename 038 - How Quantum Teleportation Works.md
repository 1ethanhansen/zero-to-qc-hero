Summary:
- Quantum teleportation is a fairly simple protocol with very interesting applications (like error correction)
- First step is to produce the [[031 - Quantum Computing for the Very Curious 1#^cb3853|state]] $$ \frac{| 00 \rangle + | 11 \rangle}{\sqrt{2}} $$ where Alice has one qubit and Bob has the other (Remember notation about [[035 - Quantum Mechanics Distilled 2#^c7adaf|tensor products]])
- Then this [[032 - Quantum Computing for the Very Curious 2#^beebd4|circuit]] is applied (Alice has the top 2 qubits and Bob has the bottom) ![](https://quantum.country/assets3/teleport.png)
- Quantum teleportation does **not** enable faster-than-light communication

Analogy:
- Quantum teleportation is like error-correcting codes for classical computing. You don't *technically* have to know it, but if you don't are you even a computer scientist?