[[046 - Qubits and Quantum States, Quantum Circuits, Measurements Part 2#^c45d07|Entanglement]] means that you can re-write the state as a product of state vectors where each state vector pertains to a specific qubit
![[Pasted image 20220620193442.png]]

States that are separable are also called product states

$$ (A \otimes B)(| a \rangle \otimes | b \rangle) = (A|a\rangle) \otimes (B|b\rangle) $$

