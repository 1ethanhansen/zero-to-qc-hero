What are the most general, physically admissable transformations of [[098 - Mixed States and Density Matrices|density operators]]?

completely positive trace-preserving maps AKA quantum channels