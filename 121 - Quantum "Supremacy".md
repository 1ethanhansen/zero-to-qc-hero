Quantum supremacy about a real-world quantum computer doing something a real-world classical computer could not (don't care if it's useful)

There exists an error rate such that if the actual error rates are less than it, you can make any provided quantum circuit fault-tolerant. That error rate is between $10^{-3}$ and $10^{-2}$. See also: https://www.metaculus.com/questions/11319/quantum-computing-tolerance-5/

Current best error rate is about $10^{-3}$




See also: https://medium.com/@wjzeng/clarifying-quantum-supremacy-better-terms-for-milestones-in-quantum-computation-d15ccb53954f