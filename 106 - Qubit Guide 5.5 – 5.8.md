![[038 - How Quantum Teleportation Works]]

General form of a controlled-[[028 - Linear Algebra Kata#^e1fe7d|unitary]] gate:
$$ c-U = |0 \rangle \langle 0 |\mathbb{1} + |1 \rangle \langle 1| U $$
which gives a 4x4 matrix looking like $$ \begin{bmatrix} \mathbb{1} & \mathbb{0} \\ \mathbb{0} & U \end{bmatrix} $$
Phase kick-back allows [[074 - Qubit Guide 1.1 – 1.4#^051a20|inducing a phase change]] on a qubit (or set of qubits) by using the qubit as a control and trying to apply a controlled unitary to another qubit in an [[045 - Qubits and Quantum States, Quantum Circuits, Measurements Part 1#^4ac87a|eigenstate]] of the unitary operator (kinda complicated, I don't fully understand it yet) ^529695

[[084 - Qubit Guide 2.5 – 2.8#^5e04e2|Clifford group]] on n-qubit circuits includes CNOTs
Clifford+T gate is universal