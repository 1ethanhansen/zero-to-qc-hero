Adjoining States:
- If rho [[098 - Mixed States and Density Matrices|density matrix]] for some d-dimensional state and sigma density matrix for another e-dimensional state, whole system d x e dimenstional
- $$ \text{density matrix is } \rho \otimes \sigma $$
trace is cyclically equivalent: $$ tr(ABC) = tr(BCA) = tr(CAB) $$
$$ tr(\langle u_i | \rho | u_i \rangle) = tr( \rho | u_i \rangle \langle u_i |) $$
POVM is Big Ol Device for Measuring, basically "quantum events"

If POVM registers outcome "i", what does rho collapse to? Depends on how POVM is implemented

said here [[101 - Qubit Guide 4.4 – 4.6#^615a63|observables]] are any old [[028 - Linear Algebra Kata#^3137ce|hermitian]] matrix X in C^{dxd}

expectation is $$ \sum_{i=1}^d < \rho, | u_i \rangle \langle u_i | > \chi_i = < \rho, \sum_{i=1}^d \chi_i | u_i \rangle\langle u_i | > = <\rho, X > $$