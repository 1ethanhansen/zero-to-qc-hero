1.10.3 Complex numbers
- a) a^{-1} is not an integer for any value of a other than 1 (for example: if a=5, a^{-1} is 1/5 which is not an integer)
- b) closed under multiplication and closed under addition
- c) 0, sqrt{2}, 2^{21}i, sqrt[4]{-1}, 
- d) not adding +/- (there's a positive and negative branch for square roots)

1.10.4 Many computational paths
my work (something's wrong here, but it's a good start I think)
![[workfor1.10.4.excalidraw.png]]

1.10.5
- a) $$ (z^2 ( 1 + e^{2i\varphi}))^2 $$
- b) 

1.10.6
$$ \begin{bmatrix}
0.5 + 0.5i & 0.5 - 0.5i \\
0.5 - 0.5i & 0.5 + 0.5i
\end{bmatrix} $$

1.10.11
1. I thought that was just convention. Can you find constants such that log of one base is always O(log_{another base}) ?
2. both
3. a) true b) true? (desmos says true) c) true

1.10.12
$$ 0.5^r $$

1.10.13
1. 2^r
2. 