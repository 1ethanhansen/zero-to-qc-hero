Summary:
- Functions operating on vectors are called Transformations
- Given a Transformation T: $$ T: \mathbb{R}^n \rightarrow \mathbb{R}^m $$
T is a linear transformation iff
1. $$ T(\vec{a} + \vec{b}) = T(\vec{a}) + T(\vec{b}) $$
2. $$ T(c\vec{a}) = cT(\vec{a}) $$
$$ \forall \ \vec{a}, \vec{b} \in \mathbb{R}^n $$

Analogy: A matrix is like a spreadsheet where each element in a matrix is like a cell in the spreadsheet