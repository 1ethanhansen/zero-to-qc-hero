[[QuantumKatas/tutorials/ComplexArithmetic/README]]

Summary:

$$ i^2 = -1 $$
$$ i^{2^2} = 1$$
$$ (a+bi)(c+di) = ac+bd(i^2) + adi + cbi = ac-bd + (ad+cb)i $$

Analogy: the modulus for complex numbers is like the absolute value for real numbers