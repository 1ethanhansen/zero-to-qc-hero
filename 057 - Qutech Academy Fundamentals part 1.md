Summary:
- A Hamiltonian for a harmonic oscillator is a matrix that assigns an energy to each of its [[045 - Qubits and Quantum States, Quantum Circuits, Measurements Part 1#^4ac87a|eigenstates]]
	- $$ H = \sum_k E_k | \psi_k \rangle \langle \psi_k | $$
	- ![[Pasted image 20220512163746.png]]
- readout operators given by $$ A = \sum_k r_k | \psi_k \rangle \langle \psi_k | $$
- average experimental outcomes given by "sandwich products": $$ \langle \phi | A | \phi \rangle = \sum_k r_k \langle \phi | \psi_k \rangle \langle \psi_k | \phi \rangle = \sum_k r_k |\langle \phi | \psi_k \rangle |^2 $$ (which is just [[045 - Qubits and Quantum States, Quantum Circuits, Measurements Part 1#^742310|The Born rule!]])
- ket notation is nice for sparse states
- if you have a lot of different amplitudes, not as nice
- What happens when you apply a Hadamard gate on the plus state in matrix representation? How does that look on the Bloch sphere?
	- $$ \frac{1}{\sqrt{2}}^2 \begin{bmatrix} 1 & 1 \\ 1 & -1 \end{bmatrix} \begin{bmatrix} 1 \\ 1 \end{bmatrix} = \frac{1}{2} \begin{bmatrix} 2 \\ 0 \end{bmatrix} = \begin{bmatrix} 1 \\ 0 \end{bmatrix} = | 0 \rangle $$
	- on the bloch sphere that is going from the equator to the north pole
- Measurement operators
	- measurement operators are matrices
	- 3 rules:
		- sandwich rule
			- calculating the probability is same as expectation value of the measurement operator
			- $$ P = \langle \psi | M_0 | \psi \rangle = \langle \psi | 0 \rangle \langle 0 | \psi \rangle = |\langle 0 | \psi \rangle|^2 $$
		- identity rule
			- sum over all measurement operators in a basis should be the identity matrix
			- $$ \sum_i M_i = \mathbb{1} $$
		- normalization
			- take a state psi, measure it with 0 operator (|0><0|)
			- resulting state phi is $$ | \phi \rangle = \frac{1}{\sqrt{p_0}} M_0 | \psi \rangle $$
- Any state phi can be expressed as $$ | \phi \rangle = \sum_k \langle \psi_k | \phi \rangle | \psi_k \rangle $$

Analogy:
(someone please correct me if this is wrong)
- A Hamiltonian is like a taylor series, you can keep adding infinite entries, but at some point it becomes good enough and adding any more is overkill