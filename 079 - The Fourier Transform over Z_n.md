Summary:
- Fourier Transform over $$ \mathbb{Z}_N $$
	- Decomposes $$ g: \mathbb{Z} \to \mathbb{C} $$ into linear combinations/strengths of discrete (co)sine functions $$ \chi_0, \chi_1, ... \chi_{N-1} $$
- When N = 2^n, this fourier transform $$ | g \rangle \overset{DFT}{\longrightarrow} \sum_{S=0}^{N-1} \hat{g}(s) | s \rangle $$  can be done by nlogn 1 and 2-qubit quantum gates
- associate g to a vector $$ g = \frac{1}{\sqrt{N}} \begin{bmatrix}
g(0) \\
g(1) \\
... \\
g(N-1)
\end{bmatrix} = \frac{1}{\sqrt{N}} \sum_x^{N-1} g(x) | x \rangle  \in \mathbb{C}^N $$
- Cool feature of XOR functions
	- $$ \chi_S(x + y) = \chi_S(x) \chi_S(y) $$
- For 0 <= S <= N: $$ \chi_S(X) = \omega_N^{SX} $$
- $$ \chi_0(x) \equiv 1 $$
- $$ \chi_S(X)^* = (\omega_N^{SX})^* $$
- $$ \chi_{-S}(X) = \omega_N^{-SX} $$
- $$ \chi_S(X) = \chi_X(S) $$
- Key fact: vectors of these XOR functions are orthonormal
- Implementing DFT_N with ~n^2 1 and 2 qubit gates
- 

Analogy: