Summary:
- **Postulate 4:** The state space of a composite physical system is the tensor product of the state spaces of the component physical systems. Moreover, if we have systems numbered 1 through n, and system number j is prepared in the state ∣ψ_j⟩, then the joint state of the total system is just the [[028 - Linear Algebra Kata#^e16522|tensor product]] of the individual states, ∣ψ1⟩⊗∣ψ2⟩⊗…⊗∣ψn⟩.
- ∣00⟩=∣0⟩∣0⟩=∣0⟩⊗∣0⟩ ^c7adaf
- when we apply unitary matrices or measurement operators to a system which is part of a larger quantum system, we extend the relevant unitary matrices and measurement operators by tensoring with the identity matrix acting on the state space for the rest of the system ^9be5b5
- ![](https://quantum.country/assets4/nutshell.png)

Analogy:
- Quantum Mechanics is like darwin's theory of evolution: a good starting point, but more work needs to be built on top of it to use it to describe more phenomena more accurately