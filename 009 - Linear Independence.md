Summary:
- A set of vectors $$ s = \{\vec{v_1}, \vec{v_2}, ... , \vec{v_n}\} $$ is linearly dependent iff $$ c_1v_1 + c_2v_2 + ... + c_nv_n = \vec{0} $$ for some c_i's that are not ALL zero
- A set of vectors V which is a subset of $$ \mathbb{R}^n $$ is a subspace of ℝ^n (for some n) iff
1. $$ \vec{0} \in V $$
2. for vector x in V $$ c\vec{x} \in V $$
3. for vector a in V and vector b in V $$ \vec{a} + \vec{b} \in V $$
- In other words, a set is a subspace if it contains the zero vector, is closed under addition, and is closed under multiplication ^8308d5

Analogy: A subspace is likehaving very specific criteria for which M&Ms you want to eat together. The set of all M&Ms is like ℝ^n and the subspace is like saying "I'll only eat a set of yellow and green M&Ms"