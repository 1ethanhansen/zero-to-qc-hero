Summary:
- Elitzur--Vaidman Bomb
	- Bomb is either dud (nothing happens) or bomb
	- if you send a 0, 0 comes out no matter what
	- if you send a 1, explodes if bomb is active
	- Send in a superposition: `|+>`
		- measure the photon in `|+>` / `|->` basis
- For any angle theta - you can build a physical device that rotates a qubit's state by theta

Analogy: