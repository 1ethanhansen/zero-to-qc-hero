Summary:
- [[034 - Quantum Mechanics Distilled 1|QM]] Law 1: If a "particle" can be in one of two basic states |0> or |1>, it can also be in a superposition of the two states $$ \alpha | 0 \rangle + \beta | 1 \rangle $$
	- called a qubit with alpha amplitude on 0 and beta amplitude on 1
	- $$ |\alpha |^2 + |\beta |^2 = 1 $$
- [[034 - Quantum Mechanics Distilled 1#^3bf56c|QM]] Law 2: For a particle in the above state, with probability |alpha|^2 the readout shows 0 and with probability |beta|^2 the readout shows 1
	- and if the readout showed 0, the [[032 - Quantum Computing for the Very Curious 2#^cd91cd|state changes]] to 1 amplitude on 0 + 0 amplitude on 1, otherwise the reverse
- Qudit state is a d-dimensional vector over the complex numbers
	- $$ \vec{v} = \begin{bmatrix}
\alpha_1 \\
\alpha_2 \\
... \\
\alpha_d
\end{bmatrix} \in \mathbb{C}^d $$
	- Because length is normalized, this has to be a [[006 - Getting Started with Vectors#^40aa0d|unit vector]]
	- Magnitude of complex number is number * [[027 - Complex Algebra Kata|conjugate]]

Analogy: