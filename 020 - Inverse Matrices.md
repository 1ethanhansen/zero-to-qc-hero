Summary:
- Matrix products are distributive
- Inverses must be unique and Inverses [[019 - Compositions of linear transformations and Matrix Products|composed]] with the function gives the identity matrix
- f(x) = y is invertible iff for all y in Y, f(x)=y has a unique solution x

Analogy: The inverse of a matrix/function is like flipping the start and end point in OpenStreetMap. You go from a path that leads from x to y, to a path that leads from y to x