Summary:
- Potential applications
	- Searching databases (grover's)
	- Finding the way out of a maze
	- Quantum approximate optimzation
- Breaking Encryption
	- Asymmetry in the complexity of multiplying and factoring
	- factoring takes O(2^n)
	- shor's algorithm is O(n^3)
	- caveats:
		- building a quantum computer is hard
		- number of qubits required is high - factoring a 2000 bit number takes ~ 10k logical qubits
- The classical one-time pad
	1. alice chooses a random key
	2. bob copies the random key
	3. alice does bitwise xor with key - creates ciphertext
	4. bob does bitwise xor of ciphertext with key
	- key can only be used **once**
- Grover's Algorithm ^eab0eb
	- Oracles - black box that is unitary, but implementation complexity unknown ^da2b6a
	- Question: if the oracle is constructed as $$ U_f = I - 2 | w \rangle \langle w | $$ where |w> is the correct state, doesn't that mean we already know what the correct state is??

Analogy: