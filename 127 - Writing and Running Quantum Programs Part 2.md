Just a walk through of a Qiskit implementation of [[126 - Writing and Running Quantum Programs Part 1]]

[[036 - How the Quantum Search Algorithm works 1|Grover's Algorithm]]
algorithm that searches unsorted database with $N = 2^n$ elements in $O(\sqrt{N})$ time. Find x such that f(x) = 1
goal: GIVEN an oracle $U_f$ with $f : \{0, 1\}^n \to \{0, 1\}$
$$ f(x) = \begin{cases}
1 & x=\omega \\
0 & else
\end{cases} $$

$$ U_f = \mathbb{I} - 2 | \omega \rangle \langle \omega | $$
