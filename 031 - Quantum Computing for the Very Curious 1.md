Summary:
- The state of a qubit is a 2D [[006 - Getting Started with Vectors|vector]] ^cb3853
- The state space of a qubit has 2 dimensions
- $$ \ket{0} := \begin{bmatrix}
1 \\
0
\end{bmatrix} $$
- $$ \ket{1} := \begin{bmatrix}
0 \\
1
\end{bmatrix} $$
- A superposition is a [[008 - Linear Combinations|linear combination]] of two states ^4a15ad
- amplitude reference to coefficients on a state in superposition
- The [[018 - Projections|norm]] of the state vector of a qubit must always be 1
- the quantum state of a qubit is a vector of unit length in a two-dimensional complex vector space known as state space

Analogy: