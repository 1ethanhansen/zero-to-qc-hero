Summary: We learned about the basics of vectors: Real Coordinate spaces, adding vectors together, multiplying vectors by a scalar, and unit vectors.
- vectors have both direction and magnitude
- real coordinate spaces are spaces with a certain dimensionality where coordinates are defined with real numbers and are denoted like this $$ \mathbb{R}^2, \mathbb{R}^3, \mathbb{R}^n $$
- to say a vector is a member of a specific space you say $$ \vec{a} \in \mathbb{R}^n $$
- to add two vectors together, you just add each row together: $$ \begin{bmatrix}
1 \\
2 
\end{bmatrix} + \begin{bmatrix}
3 \\
4
\end{bmatrix} = \begin{bmatrix}
4 \\
6
\end{bmatrix}$$
- When you multiply a vector by a scalar, you multiply all components of the vector by the scalar: $$ \begin{bmatrix}
1 \\
2 
\end{bmatrix} * 3 = \begin{bmatrix}
3 \\
6 
\end{bmatrix} $$
	- scalar multiplication (by a positive number) doesn't change the direction of a vector, just the magnitude
	- scalar multiplication changes the direction, but it stays along the same line (points in opposite direction)
- You define 1 unit vector for each dimension you're operating in (2 unit vectors in 2-d, 3 in 3-d, etc)
- Unit vectors get hats $$ \hat{i} = \begin{bmatrix}
1 \\
0 
\end{bmatrix}$$
$$ \hat{j} = \begin{bmatrix}
0 \\
1 
\end{bmatrix} $$ ^40aa0d
- You can represent other vectors as sums of unit vectors: $$ \vec{v} = \begin{bmatrix}
1 \\
2 
\end{bmatrix} = \hat{i} + 2\hat{j} $$

Analogy: A vector is like the instructions on a treasure map (take 3 steps southeast, take 4 steps north) in that it has both direction (for a treasure map: north, south, east, west) and a magnitude (for a treasure map: the number of steps)