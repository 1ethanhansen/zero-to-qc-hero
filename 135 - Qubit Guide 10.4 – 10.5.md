[[077 - Qutech Academy Quantum Algorithms part 1#^da2b6a|Oracle]] model: "For this purpose, we ignore everything that happens inside the black box: the Boolean function evaluation counts as just one computational step."

Bernstein-Vazirani Problem:
$$ f : \{ 0, 1 \}^n \to \{ 0, 1 \}$$ such that $f(x) = a \cdot x$
task: find the bitstring a