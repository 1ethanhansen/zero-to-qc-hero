Summary:
- states u and -u are indistinguishable (no physical experiment can distinguish them)
- states u and iu are indistinguishable
- in general: |u> and c|u> are indistinguishable when c is a complex number with |c| = 1

Analogy:
imagine a black box - completely shut off from the outside world - and I tell you there's a coin in there that's either heads or tails. you know that the coin inside has a state, but because there's nothing you can do to determine the state, it really doesn't matter to you.