Summary:
- Eigenvectors of a matrix are those vectors that are only scaled by the matrix, not rotated at all. Eigenvalues are the values that the eigenvectors are scaled by ^c57da5
- to quickly find eigenvectors of a matrix, you can use this formula $$ A = \begin{bmatrix}
a & b \\
c & d
\end{bmatrix} \rightarrow \lambda_1, \lambda_2 = \frac{1}{2}tr(A) \pm \sqrt{(\frac{1}{2}tr(A))^2 - det(A)} $$
- Abstract vector spaces allow us to describe basically everything as matrices

Analogy: Using a massive matrix to find the derivative of a polynomial is like using a .50 BMG to hunt squirrels: it gets the job done. And like an old robotics mentor of mine said, "There's no kill like overkill"