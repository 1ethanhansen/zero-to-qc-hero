Summary:
- If you just measure the first qubit in a system described by $$ \alpha_{00} | 00 \rangle + \alpha_{01} | 01 \rangle + \alpha_{10} | 10 \rangle + \alpha_{11} | 11 \rangle $$, [[042 - Understanding and Measuring One Qubit|readout]] giving a **0** is probability $$ p_0 := |a_{00}|^2 + |a_{01}|^2 $$
	- resulting state is $$ \frac{\alpha_{00} | 00 \rangle + \alpha_{01} | 01 \rangle}{\sqrt{p_0}} $$
	- a "classical" probability distribution over (pure) quantum states = **mixed state**
	- 
 
Analogy: