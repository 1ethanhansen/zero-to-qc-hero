Summary:
- Fourier transform just has another orthonormal basis for our state space and we get the coordinates of some data in terms of that other orthonormal basis
- Think of a vector of length N=2^n as a mapping `f: {0, 1}^n -> C`
- 

Analogy: