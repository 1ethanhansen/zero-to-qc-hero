Summary:
- Postulate 1: Associated to any physical system is a complex [[009 - Linear Independence|vector space]] known as the state space of the system. If the system is isolated, then the system is completely described by its state vector, which is a [[007 - Vector Practice|unit vector]] in the system's state space
- Postulate 2: The evolution of an isolated quantum system is described by a unitary matrix acting on the state space of the system. That is, the state ∣ψ⟩ of the system at a time t_1 is related to the state ∣ψ′⟩ at a later time t_2 by a unitary matrix, U: ∣ψ′⟩=U∣ψ⟩. That matrix U may depend on the times t_1 and t_2, but does not depend on the states ∣ψ⟩ and ∣ψ′⟩.
- The Hamiltonian of a system is a [[028 - Linear Algebra Kata#^3137ce|hermitian]] matrix that describes time evolution in the Schrodinger equation
- **Postulate 3:** Quantum measurements are described by a collection {M_m} of _measurement operators._ Each M_m is a matrix acting on the state space of the system being measured. The index _m_ takes values corresponding to the measurement outcomes that may occur in the experiment. If the state of the quantum system is ∣ψ⟩ immediately before the measurement then the probability that result _m_ occurs is given by $$ p(m) = \langle \psi| M_m^\dagger M_m |\psi\rangle $$ and the state of the system after the measurement, often called the _posterior state_, is $$ \frac{M_m|\psi\rangle}{\sqrt{\langle \psi|M_m^\dagger M_m |\psi\rangle}} $$ (It's worth noting that: (a) the denominator is just the square root of the probability p(m); and (b) this is a properly normalized quantum state.) The measurement operators satisfy the _completeness relation_ $$ \sum_m M_m^\dagger M_m = I $$ ^3bf56c

Analogy:

Questions:
If $$ M_0 = |0 \rangle \langle 0 | $$, why is $$ M_0^\dagger = |0 \rangle \langle 0 | $$ instead of $$ M_0^\dagger = \langle 0 | 0 \rangle $$
