Summary:
- $$ cos(\theta) = \frac{\vec{a} \cdot \vec{b}}{||\vec{a}||||\vec{b}||} $$
- if the [[010 - Bases and Dot products#^39f0f7|dot product]] of two (non-zero) vectors is 0, the vectors are perpendicular
- $$ \vec{a} \cdot \vec{b} = 0 \iff orthogonal $$ ^7f8b3e
- Cross product only defined in ℝ^3
- $$ \vec{a} = \begin{bmatrix}
a_1 \\
a_2 \\
a_3
\end{bmatrix} ,\ \ \ \vec{b} = \begin{bmatrix}
b_1 \\
b_2 \\
b_3
\end{bmatrix} $$
- $$ \vec{a} \times \vec{b} = \begin{bmatrix}
a_2b_3 - a_3b_2 \\
a_3b_1 - a_1b_3 \\
a_1b_2 - a_2b_1
\end{bmatrix} $$
- cross product of two vectors is orthogonal to both vectors ^9c8798

Analogy: I can't really think of anything other than orthogonality is an extension of perpendicularity like dark matter is an extension of physics: useful, but if you ask anyone what it is they'll kinda shrug and go "it's a useful fiction."