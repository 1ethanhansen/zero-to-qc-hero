Summary: 
- Matrix multiplication is just applying one transformation then another transformation
- 3-d matrices are like 2-d matrices but in 3-d (*insert applause soundtrack here*)
- The determinant can be thought of as how much area/volume is scaled for a transformation for 2/3-d space (respectively)
- The inverse of a matrix can be thought of as rewinding the transformation that the original matrix did

Analogy:
