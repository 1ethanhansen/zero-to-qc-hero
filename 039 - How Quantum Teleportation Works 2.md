Continued from [[038 - How Quantum Teleportation Works]]

Summary:
- Partial measurement: when you measure just one qubit, other qubits remain in the possible states where the probability is re-normalized so it's a valid quantum state ^7eed50
1.  **Initial state:** Alice starts with a quantum state ∣ψ⟩ of a single qubit. Alice and Bob also share a quantum state of two qubits, (∣00⟩+∣11⟩)/\sqrt{2}
2.  **What Alice does:** To accomplish her part of the protocol, Alice performs a CNOT gate between ∣ψ⟩ and her other qubit, then applies a [[032 - Quantum Computing for the Very Curious 2#^31447f|Hadamard]] gate to the first qubit. Alice then measures both her qubits in the computational basis, getting results z = 0 or 1 and x = 0 or 1. The probability for each of the four possible measurement outcomes (00, 01, 10, 11) is 1/4.
3.  **Classical communication:** Alice sends both classical bits, z and x, to Bob.
4.  **Bob recovers the quantum state ∣ψ⟩:** Bob applies Z^z X^x to his qubit, recovering the original state ∣ψ⟩.

Analogy: Quantum Teleportation only teleports "the soul" of the quantum state. The quantum state doesn't appear instantly, it takes some manipulation on Bob's end using the classical information to appear