Summary:
- Rotation around x-axis in R^3 is basically [[017 - Linear Transformations#^e0179b|rotation in R^2]] + a column for the x-dimension which doesn't change $$ \begin{bmatrix}
1 & 0 & 0 \\
0 & cos\theta & -sin\theta \\
0 & sin\theta & cos\theta
\end{bmatrix} $$
- norm of a vector = length of a vector
- $$ proj_L(\vec{x}) $$ is the vector in L where $$\vec{x} - proj_L{\vec{x}}$$ is orthogonal to L
- The matrix that transforms a vector into it's projection onto a line defined by the unit vector $$ \hat{u} = \begin{bmatrix}
u_1 \\
u_2
\end{bmatrix} $$ is $$ \begin{bmatrix}
u_1^2 & u_1u_2 \\
u_1u_2 & u_2^2
\end{bmatrix} $$

Analogy: