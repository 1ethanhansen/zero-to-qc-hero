Summary: reviewed from [[006 - Getting Started with Vectors|yesterday's content]]
A unit vector in the direction of another vector (v) is $$ \frac{\vec{v}}{|| \vec{v} ||} $$

Analogy: if we look at just a number line and take a number, when we ask "how many units are in this number," to find the answer we divide by the magnitude. So if the question is "how many units in -6" we divide by 6. Similarly for a vector to get a unit from the vector, just divide the vector by its magnitude.