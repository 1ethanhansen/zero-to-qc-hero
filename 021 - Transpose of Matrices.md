Summary:
- $$ A = \begin{bmatrix}
a_{11} & a_{12} & ... & a_{1n} \\
a_{21} & a_{22} & ... & a_{2n} \\
... & & & \\
a_{m1} & a_{m2} & ... & a_{mn}
\end{bmatrix} $$
- $$ A^{\top} = \begin{bmatrix}
a_{11} & a_{21} & ... & a_{m1} \\
a_{12} & a_{22} & ... & a_{m2} \\
... & & & \\
a_{1n} & a_{2n} & ... & a_{mn}
\end{bmatrix} $$
- $$ det(A^\top) = det(A)$$
- transpose of a vector changes a vector from column vector to row vector and vice-versa
- $$ \vec{v} \cdot \vec{w} = v^\top \vec{w} $$

Analogy: Taking the transpose (especially of a nxn matrix) is like flipping everything over the diagonal that goes from the top left to the bottom right