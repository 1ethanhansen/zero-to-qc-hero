Summary:
- A linear combination is addition of [[006 - Getting Started with Vectors|vectors]] where the vectors are each scaled by a real number $$ a\vec{v} + b\vec{w} \ \ \ for \ a, b \in \mathbb{R} \ \ \ and \ \ \ \vec{v}, \vec{w} \in \mathbb{R}^m $$
- Span is the set of all vectors that can be created with a linear combination of a set of other vectors ^51f3ae
- A set of vectors is [[009 - Linear Independence|linearly independent]] if none of the vectors in the set can be defined by linear combinations of other vectors in the set

Analogy: A linearly dependant set of vectors is like watching A New Hope and The Force Awakens because all the ground was already covered in A New Hope so there's really no need for The Force Awakens

don't @ me