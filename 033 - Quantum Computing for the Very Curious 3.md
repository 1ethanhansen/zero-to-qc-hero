Continuation from [[032 - Quantum Computing for the Very Curious 2|part 2]]

Summary:
- For the identity matrix, you can multiply it by $$ e^{i\theta} $$ and it affects the state in the same way. the constant is called the "global phase factor" and it does not affect the measurement probabilities. ^eacf1c

Analogy:
- quantum computers might not be able to simulate certain physical systems like classical computers are unable to simulate quantum mechanical systems (efficiently)