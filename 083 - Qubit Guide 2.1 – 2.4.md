Summary:
- Quantum operations enacted by [[028 - Linear Algebra Kata#^e1fe7d|unitary]] matrices
- Qubits are 2-level systems
- Using the [[046 - Qubits and Quantum States, Quantum Circuits, Measurements Part 2#^1170da|circuit model]] of quantum computation
- Single qubit interference ^159ec1
	- ![[Pasted image 20220527094854.png]]
	- Basically same as [[074 - Qubit Guide 1.1 – 1.4#^a04a80|ramsey interferometry]]

Analogy: