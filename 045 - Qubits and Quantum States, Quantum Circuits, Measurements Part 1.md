Summary:
- after measurement, qubit collapses to an eigenstate ^7a0840
	- eigenstate is just another name for normalized [[026 - 3Blue1Brown LinAlg day 5#^c57da5|eigenvector]] of the given matrix ^4ac87a
- $$ \langle a | b \rangle = \overline{\langle b | a \rangle} $$ (using my linear algebra notation overline = complex conjugate)
- Born rule: the probability that psi collapses during a projection measurement onto the basis {|x>, |x^perp>} to state |x> is given by $$ p(x) = | \langle x | \psi \rangle |^2 $$ ^742310
- Any [[034 - Quantum Mechanics Distilled 1#^3bf56c|normalized]], pure state can be written as $$ | \psi \rangle = cos \frac{\theta}{2} | 0 \rangle + e^{i\varphi} sin \frac{\theta}{2} | 1 \rangle $$
	- phi describes the relative phase and theta is the probability to measure 0/1
- Opposite-side states on the bloch sphere are orthogonal in Hilbert Space

Analogy:
- 