Summary:
- most important characteristic for a good qubit is controllability [[031 - Quantum Computing for the Very Curious 1|(mentioned in QC for the Very Curious)]]
- I think the answer to the "further thinking" on [what is superposition?](https://www.qutube.nl/quantum-101/what-is-superposition) is wrong
	- $$ (\frac{1}{2})^5 = \frac{1}{32} \neq \frac{1}{25} $$
- Creating entanglement: move two particles close together, do an operation, move apart
	- monogamy of entanglement: if two qubits are maximally [[046 - Qubits and Quantum States, Quantum Circuits, Measurements Part 2#^c2f6b4|entangled]], no other particle can have a share
	- maximal coordination: when two entangled qubits are measured in the same basis, they will always yield a coordinated outcome
- If you [[045 - Qubits and Quantum States, Quantum Circuits, Measurements Part 1#^742310|measure one qubit]] in an entangled pair in one basis and the other in another basis, the results will be entirely uncorrellated
- No-cloning: given an unknown quantum state, no way to reliably create a copy of the state (without destroying the original state) ^9cbe04
- Current state of quantum computing
	- NISQ - Noisy Intermediate Scale Quantum
	- types: trapped ion, silicon, superconducting, nv centers, majorana quasiparticle

Analogy:
- [[031 - Quantum Computing for the Very Curious 1#^4a15ad|superposition]] is like waves