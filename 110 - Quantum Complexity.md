BQP is the complexity class of algorithms that can be solved efficiently by quantum computers


BQP is short of Bounded-error Quantum Polynomial

BPP is same as BQP except circuit Cn is a classical circuit with [[041 - Rotate, Compute, Rotate|randomness]] allowed (coin flip gates)

P is same but no coin flips (so no bounded error anymore either)

Generally believed that BPP = P

NP is all the decision problems phi such that there's a randomized/probabilistic polynomial-time algorithm A such that
1. for all inputs x st phi(x) = YES, pr(A(x) = 1) > 0
2. for all inputs x st phi(x) = NO, pr(A(x) = 1) = 0

Believed that SAT not in BQP

coNP is NP but conditions are reverse

QMA is quantum version of NP

PP (wrong definition of BPP)
1. for all inputs x st phi(x) = YES, pr(A(x) = 1) > 1/2
2. for all inputs x st phi(x) = NO, pr(A(x) = 1) < 1/2
PP contains BPP and NP and QMA

![[complexityclasses.excalidraw.png]]