[[077 - Qutech Academy Quantum Algorithms part 1#^da2b6a|oracles]] we have a physical device that we can't look inside but we can pass queries and get results
our goal is to determine some property of the oracle using the minimal number of queries
phone book example of an oracle is a bad example (not feasible)
better example is satisfiability problem (like sudoku)

for $x \in \{ 0, 1 \}^n$ applying n hadamard gates to the n qubits gives
$$ H^{\otimes n} | x \rangle = \frac{1}{\sqrt{2^n}} \sum_{k \in \{ 0, 1 \}^n } (-1)^{k \cdot x} | k \rangle $$

We are given an oracle that is either constant or balanced
task: figure out which type it is
classical way is just test everything, worst-case run time is N/2 queries
quantum solution only ever needs 1 query

