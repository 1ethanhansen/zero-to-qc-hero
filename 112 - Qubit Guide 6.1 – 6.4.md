See also [[098 - Mixed States and Density Matrices]]

density matrix AKA density operator

![[046 - Qubits and Quantum States, Quantum Circuits, Measurements Part 2#^c2f6b4]]

A density operator $\rho$ on a finite dimensional Hilbert space $\mathscr{H}$ is any non-negative self-adjoint operator with trace equal to 1.
Partial Trace: $$ tr_B(A \otimes B) = ( \mathbb{1} \otimes tr)(A \otimes B) = A tr(B) $$ $$ tr_A(A \otimes B) = ( tr \otimes \mathbb{1} )(A \otimes B) = tr(A) B $$
density operators can be combined $$ \rho_3 = p_1\rho_1 + p_2\rho_2 \ | \ p_1, p_2 \geq 0 \ and \ p_1 + p_2 = 1 $$
Pure states can be expressed $\rho = | \psi \rangle \langle \psi |$

For any entangled state there is a Schmidt Decomposition $|\psi \rangle = \sum_k d_k | a_k \rangle |b_k \rangle$ 
A pure state can be seen as a special case of a mixed state, where all but one the probabilities equal zero.
In general, two different mixtures can be distinguished (in a statistical sense) if and only if they yield different density matrices. In fact, the optimal way of distinguishing quantum states with different density operators is still an active area of research.

## 6.3
### 4
I think measure in psi, psi perp basis. because in scenario 2 you should always get psi, but if the qubit was prepared scenario 3 you might get psi perp. If you ever find psi perp, you know it can't be scenario 2. So perform lots and lot of measurements and then you can have high confidence one way or the other

off-diagonal elements of density matrix are called **coherences** and the process by which they go to 0 is [[075 - Qubit Guide 1.5 – 1.9#^b4cb0e|decoherence]]
Sometimes convenient to write density operators in terms of states that are not normalized, but instead incorporate probabilities of the states into the states themselves: $$ \rho = \sum_i |\tilde{\psi_i} \rangle \langle \tilde{\psi_i} | $$ where $|\tilde{\psi_i}\rangle = \sqrt{p_i}|\psi_i\rangle$ (implies $p_i = \langle \tilde{\psi_i} | \tilde{\psi_i} \rangle$)

