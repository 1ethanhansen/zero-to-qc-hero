Summary:
- A matrix transformation of 2-d vectors can be writted as two column vectors combined where the first column is where the i vector lands and the second column is where the j vector lands

Analogy: in the material there was an analogy to the number line. Like how you can take the number 1 and see where it lands when multiplied by a real number (a linear transformation in 1-d space), you can also follow the unit vectors for 2-d space and see where they land to know what the transformation is