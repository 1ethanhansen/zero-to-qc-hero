Summary:
- Amazing aspect: quantum algorithm with [[040 - 10 to the 500 Parallel Universes#^abac1e|exponential speedup]] over classical
- `F: {0, 1]^n -> {0, 1}^m`
	- m >= n
	- like to think of F's outputs as "colors"
	- so `F: {0, 1]^n -> COLORS subseteq {0, 1}^m`
- F is promised to be "L-periodic" for some secret string `L in {0, 1}^n`
	- F is L-periodic if for L != 00...0 if for all x, F(x+L) = F(x)
- Simon's problem:
	- Given black-box access to Q_F (implements L-periodic function F), determine L
	- Classically really hard (exponential)
	- Quantumly need <= 4n applications of Q_F
- Loading Data
	- n qubits for input register, m qubits for output register
	- inputs start all |+>, output registers start all 0s
		- overall state: $$ \frac{1}{\sqrt{N}} \sum_x |x \rangle \otimes |0^m\rangle $$
	- Apply Q_F and get $$ \frac{1}{\sqrt{N}} \sum_x | x \rangle \otimes | F(x) \rangle $$
	- measure the m outputs
		- outcome in output register is some random color with probability 2/N
		- state collapses to superposition of only the input states corresponding to the measured output
		- [[032 - Quantum Computing for the Very Curious 2#^cd91cd|collapses to]] $$ (\frac{1}{\sqrt{2}} (|x^* \rangle + |x^* + L \rangle))\otimes | C^* \rangle $$
- Apply all [[032 - Quantum Computing for the Very Curious 2#^31447f|Hadamards]] to input register
- Measure the input register gives some state S such that s dot L = 0
- 

Analogy:
- 