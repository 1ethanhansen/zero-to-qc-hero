Summary:
- given state $$ | \psi \rangle = \{ |u \rangle, |v \rangle \} $$
	- (state is either u or v for sure)
- Error for two-sided error algo is $$ Pr[error] = 1/2 - 1/2 sin \theta $$ where theta is the angle between states u and v

Analogy: