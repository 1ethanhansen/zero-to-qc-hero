Summary: 
- This series is supposed to give a geometric intuition for linear algebra
- Vectors can be thought of as instructions for motion in space (or plane) and addition and multiplication operates like you would expect for adding instructions together or multiplying instructions by a number
- A linear combination of vectors is just vectors multiplied by scalars added together
- See [[014 - Visualizing Transformations]]

Analogy:
Vectors can be thought of as instructions for motion in space (or plane)