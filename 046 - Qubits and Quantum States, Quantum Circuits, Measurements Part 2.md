Summary:
- Circuit model of quantum circuits ^1170da
- quantum gates are represented with [[028 - Linear Algebra Kata#^e1fe7d|unitary]] matrices (U\*U = I\_n, unitary matrices are norm-preserving)
- $$ u = \begin{bmatrix} u_{00} & u_{01} \\ u_{10} & u_{11} \end{bmatrix} = u_{00} |0 \rangle \langle 0 | + u_{01} |0 \rangle \langle 1 | + u_{10} |1 \rangle \langle 0 | + u_{11} |1 \rangle \langle 1 |$$
- $$ \sigma_x = |0 \rangle \langle 1 | + |1 \rangle \langle 0 | $$
- $$ \sigma_z = |0 \rangle \langle 0 | - |1 \rangle \langle 1 | $$
- Pauli matrices form a [[010 - Bases and Dot products#^fb1f98|basis]] of 2x2 matrices
- [[028 - Linear Algebra Kata#^e16522|tensor products]] describe multi-partite states ^c45d07
	- $$ | a \rangle \otimes | b \rangle = \begin{bmatrix} a_1 \\ a_2 \end{bmatrix} \otimes \begin{bmatrix} b_1 \\ b_2 \end{bmatrix} = \begin{bmatrix} a_1b_1 \\ a_1b_2 \\ a_2b_1 \\ a_2b_2 \end{bmatrix} $$
	- states that can be written as $$ |\psi\rangle_A \otimes |\psi\rangle_B $$ are called uncorrelated. If they can't be written like that, they are correlated and sometimes **entangled**  ^c2f6b4

Analogy:
The circuit model of quantum computing is much like how boolean functions can be represented with logic gates and circuit diagrams