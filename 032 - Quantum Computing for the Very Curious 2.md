Continuation of [[031 - Quantum Computing for the Very Curious 1|the same essay from yesterday]]
Summary:
- X gate applies the transformation $$ \begin{bmatrix}
\alpha \\
\beta
\end{bmatrix} \to \begin{bmatrix}
\beta \\
\alpha
\end{bmatrix} $$
- The matrix for that is $$ \begin{bmatrix}
0 & 1 \\
1 & 0
\end{bmatrix} $$
	- (which should make sense based on what we know about [[014 - Visualizing Transformations]])
- Putting two quantum gates together corresponds to [[019 - Compositions of linear transformations and Matrix Products|matrix multiplication]] ^beebd4
- Hadamard matrix: $$ H = \frac{1}{\sqrt{2}} \begin{bmatrix}
1 & 1 \\
1 & -1
\end{bmatrix} $$ ^31447f
- The quantum state equivalent of the classical bit you measure is called the "posterior state" ^cd91cd
- a general single-qubit gate can be represented as a 2×2 [[028 - Linear Algebra Kata#^e1fe7d|unitary]] matrix
- X, Y, and Z are the **Pauli Matrices**
	- $$ Y := \begin{bmatrix}
0 & -i \\
i & 0
\end{bmatrix} $$
	- $$ Z := \begin{bmatrix}
1 & 0 \\
0 & -1
\end{bmatrix} $$ ^3e59fb
- unitary matrices preserve the length of their inputs
- bras are row vectors that are the adjoint of the corresponding column vectors

Analogy:
Just gonna steal the "quantum computers allow you to travel in a different way through space" analogy from the essay

Questions: 