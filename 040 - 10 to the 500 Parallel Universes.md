Summary:
- Quantum Computing is the intersection of Physics, Math, and CS
- Physical vs Unphysical Numbers ^abac1e
	- Physical: numbers that can conceivably count things happening in our universe
	- Unphysical: things like 10^500

Analogy:
- Quantum computers allow us to "rotate, compute, and rotate" so we can do things that would normally be unphysical time to be physical time