Summary:
- Computation is a physical process
- open superposition -> phase change -> close superpostion seems to be pretty equivalent to [[041 - Rotate, Compute, Rotate]] ^051a20
- Ramsey interferometry ^a04a80
	- From the classical probability theory perspective the resonant interaction induces a random switch between ∣0⟩ and ∣1⟩ (why? - because in classical everything is discrete. the particle is either in one state or the other, we just don't know which so we assign probability 1/2 to a switch) and the dispersive interaction has no effect on these two states (why? - again, in the classical there's a probability it's in one or the other, and the dispersive interaction has exactly 0 chance of causing a switch to happen). Hence, one random switch followed by another random switch gives exactly a single random switch, which gives 1/2 for the probability that input ∣0⟩ becomes output ∣1⟩.

Analogy: