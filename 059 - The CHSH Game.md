Summary:
1. Alice and Bob start each with a qubit in |0> state
2. entangle qubits into EPR pair (`1/\sqrt{2}(|00> + |11>)`)  ^2dc166
3. Alice measures in one of two bases, Bob also measures in one of two bases
	- ![[Pasted image 20220513151642.png]]
	- Alice measures in red or yellow basis, bob measures in orange or green basis
4. Alice and Bob are trying to match up their outputs (either both solid or both dotted)
5. Referees call out to alice and bob at the same time two different (or maybe same!) random bases (red/yellow for alice and green/orange for bob)
6. Alice and Bob then reply withing 10 seconds the outcome (either dotted or solid)
- best correlation alice and bob can get in this game is 75% with "local hidden variables"
- with quantum entanglement, they can get up to 85%!!

Analogy:
- The CHSH game is a game sort of like Coway's Game of Life is a game. Not very fun to play (unless you go to http://lifecompetes.com/) but because you can sort of see some adversarial aspects, we can call it a game