Summary:
- you can tranform vectors between dimensions [[013 - Transformations]] showed us that a mxn matrix goes from n dimensions to m dimensions
- [[010 - Bases and Dot products#^39f0f7|dot products]] can be thought of as taking the projection of one vector onto the other, then multiplying the length of the projection by the length of the other vector
- [[011 - Angle and Cross Products#^9c8798|cross product]] produces a 3rd vector in 3-d space that is perpendicular to the first two vectors with length = the area of the parallelogram defined by the vectors

Analogy: