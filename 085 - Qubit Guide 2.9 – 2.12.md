Summary:
- Any unitary operation (ignoring [[033 - Quantum Computing for the Very Curious 3#^eacf1c|global phase factor]]) on a qubit can be implemented by phase shift, hadamard, phase shift, hadamard, phase shift
- Unitary matrices acting on one qubit can be thought of as a rotation of some amount around some vector
- We can say that a state is "stablized" by the matrix that has that state as an [[045 - Qubits and Quantum States, Quantum Circuits, Measurements Part 1#^7a0840|eigenstate]] with [[026 - 3Blue1Brown LinAlg day 5#^c57da5|eigenvalue]] 1
- The first phase shift enacts a rotation by alpha around the z axis, the middle bit that looks like ![[083 - Qubit Guide 2.1 – 2.4#^159ec1]] gives a rotation by phi around the x axis, then the last phase shift is a rotation by beta around the z axis
	- Alpha, beta, and phi are known as the three **Euler's angles**