Continued from [[036 - How the Quantum Search Algorithm works 1]]
Summary:
- $$ C_s $$ is a black box circuit that checks if the solution is a valid solution to the problem, returning 1 if yes and 0 if no
- C\_s is also called an oracle
- in general, the search register is in some superposition $$ \sum_x a_x | x \rangle $$ and C\_s changes the superposition: $$ \sum_x a_x | x \rangle | 0 \rangle \to \sum_x a_x | x \rangle | s(x) \rangle $$
- Uncomputation is important to leave the working qubits alone instead of bringing them along for the ride
- The steps of the search algorithm, broadly, are
	1. start in the equal superposition state
	2. repeat these steps for sqrt(N) loops
		1. reflect the state about the solution state
		2. reflect the state about the equal superposition state

Analogy:
- Uncomputation is like erasing part of a whiteboard: you still want to leave up the answer at the bottom and your Space Invaders high score at the top, but the rest you want to use for scratch work again