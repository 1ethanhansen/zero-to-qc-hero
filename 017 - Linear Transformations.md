Summary:
- matrix addition is just adding up all corresponding entries
- scalar matrix multiplication is just multiplying all entries in the matrix by the scalar
- matrices that just reflect and scale are diagonal matrices
- a rotation in r2 of a specific angle can be done with the matrix $$ \begin{bmatrix}
cos\theta & -sin\theta \\
sin\theta & cos\theta
\end{bmatrix} $$ ^e0179b

Analogy: matrix addition and scalar multiplication is like repeated vector addition and scalar multiplication