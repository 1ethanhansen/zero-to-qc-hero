[[QuantumKatas/tutorials/LinearAlgebra/README]]
Summary: 
$$ \begin{bmatrix}
1 & 2 & 3 \\
4 & 5 & 6
\end{bmatrix} \begin{bmatrix}
1 & 2 \\
3 & 4 \\
5 & 6
\end{bmatrix} $$
- Hermitian = self-adjoint ^3137ce
- unitary = inverse is the same as adjoint ^e1fe7d
- inner product between two vectors is the adjoint of the first multiplied by the second
- outer product between two vectors is the first multiplied by the adjoint of the second
- tensor product multiplies the second matrix by every element of the first matrix ^e16522

Analogy: 