### 4.12.1 Projector?
in order for it to be a projector, it needs to be hermitian and idempotent.
$$ (| a \rangle \langle a | + | b \rangle \langle b |)^\dagger = (| a \rangle \langle a |)^\dagger + (| b \rangle \langle b |)^\dagger = | a \rangle \langle a | + | b \rangle \langle b |$$
so yes, checks the box for hermitian

What about idempotent?
$$ (| a \rangle \langle a | + | b \rangle \langle b |)(| a \rangle \langle a | + | b \rangle \langle b |) = | a \rangle \langle a | a \rangle \langle a | + | a \rangle \langle a | b \rangle \langle b | + | b \rangle \langle b | a \rangle \langle a | + | b \rangle \langle b | b \rangle \langle b | $$
well since a and b are unit vectors, their norm is 1 and the above simplifies to
$$ | a \rangle \langle a | + | a \rangle \langle a | b \rangle \langle b | + | b \rangle \langle b | a \rangle \langle a | + | b \rangle \langle b | $$
$$ = | a \rangle \langle a | + \langle a | b \rangle | a \rangle \langle b | + \langle b | a \rangle | b \rangle \langle a | + | b \rangle \langle b | $$
The only way I can see for that to simplify to the original matrix is if a and b are orthogonal. So if a, b orthonormal, then yes it is a projector. Otherwise, no.

### 4.12.2 Knowing the unknown
1. No, you cannot
2. That it wasn't |1>
3. You need 4 real parameters (3 if you ignore global phase). Yes, you could reconstruct psi up to a global phase factor with those 3 values
4. Measure the state in multiple bases over and over again. You couldn't perfectly reconstruct the state but you could get close. Referred to as state tomography I believe. 

### 4.12.6 Alice knows what Bob did
~~When alice gets the qubit back it would be collapsed to one specific state, and the probability that measuring it will give another state is now 0. So alice can now measure in whatever orthogonal basis~~
No I think that's wrong... not sure about this one