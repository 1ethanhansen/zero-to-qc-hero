You're given a function F in the form of a quantum circuit Q_F that implements F
Query Complexity Model
- Q_F is considered black box/[[077 - Qutech Academy Quantum Algorithms part 1#^da2b6a|oracle]] (only allowed to apply it) think of it like a magic box someone gives you
- cost of an algorithm to solve phi is _just_ the # of times you apply Q_F
- all other computation "free"
Why study this model?
- all quantum algorithms fit this model
- usually the ignored computation is cheap
- simple model - feasible to prove lower bounds