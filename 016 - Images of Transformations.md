Summary: 
- if you have an image in a space and apply a transformation, the transformation of the image follows the same "pattern" as the transformation of the vectors that define the image
- when you take the transformation (T) of an entire n-dimensional space, the image of T is the set of all vectors that R^n maps to
- Transformations can be multiplied by scalars and added together (because they are matrices)

Analogy: 