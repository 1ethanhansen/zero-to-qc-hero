Summary:
- $$ || \vec{a} \times \vec{b} || = ||\vec{a}||||\vec{b}||sin\theta $$
- Intuition of dot product: how much two vectors are moving together
- intuition of magnitude of cross product: how close two vectors are to perpendicular
	- magnitude of cross product is maximized when vectors are perpendicular
- A function is something that maps elements in one set to elements of another set
	- The set mapped from is the Domain
	- The set mapped into is the Co-Domain
	- The subset of the codomain that the function can actually map to is the Range

Analogy: A function is like an algorithm in computer science. In fact, we call "functions" functions in most programing languages for that reason (lookin at you Java). A function takes in an input and produces an output, much like an algorithm.