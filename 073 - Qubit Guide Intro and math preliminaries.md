Summary: 
- [[006 - Getting Started with Vectors]]
- vector space is a mathematical structure in which the notion of linear combination “makes sense”
- $$ \langle i | A^\dagger | j \rangle = \langle j | A | i \rangle^* $$
- $$ tr | b \rangle \langle a | = \sum_k \langle e_k | b \rangle \langle a | e_k \rangle = \sum_k \langle a | e_k \rangle \langle e_k | b \rangle = \langle a | \mathbb{1} | b \rangle = \langle a | b \rangle $$
- useful identities: https://qubit.guide/0.9-some-useful-identities.html

Analogy: