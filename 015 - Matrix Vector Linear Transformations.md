Summary:
- Matrix-vector products are always linear transformations
- The identity matrix is all zero except for the top left-bottom right diagonal which is 1
- multiplying any vector by the identity matrix results in the original vector

Analogy: 