Summary:
- Solving linear equations with quantum computers
	- HHL Algorithm
	- solve linear systems exponetially faster than classical counterpart
	- solution vector not yielded (like [[078 - Simon's algorithm]])
	- Preparation of the input vector is complicated
- Quantum Machine Learning
	- Priciple component analysis can be sped up on a quantum computer
- Simulating Chemistry
	- Model of quantum system is hamiltonian
		- Large matrix containing information about basic parts of the system
	- Takes exponential time on classical computer
	- quantum computers have exponential complexity which matches system we're simulating
	- Only certain properties can be simulated efficiently
- Deutsch – Jozsa algorithm ^9dec40
	- 

Analogy