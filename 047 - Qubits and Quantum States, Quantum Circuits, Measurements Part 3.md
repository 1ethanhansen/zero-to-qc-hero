Summary:
- bell states
	- 4 maximally-entangled states that build an orthonormal [[010 - Bases and Dot products#^fb1f98|basis]] (a basis of vectors that are both [[011 - Angle and Cross Products#^7f8b3e|orthogonal]] and [[039 - How Quantum Teleportation Works 2#^7eed50|normalized]])
	- $$ | \psi^{00} \rangle := \frac{1}{\sqrt{2}}(| 00 \rangle + | 11 \rangle $$
	- $$ | \psi^{ij} \rangle = (I \otimes \sigma_x^j \sigma_z^i) | \psi^{00} \rangle $$
- Bell measurement: do the reverse order of preparing a bell state
	- CNOT, then hadamard
	- un-does the bell state preparation
	- classical outputs i, j correspond to state collapsing to psi^{ij} state
- [[038 - How Quantum Teleportation Works]] and [[039 - How Quantum Teleportation Works 2]]

Analogy: