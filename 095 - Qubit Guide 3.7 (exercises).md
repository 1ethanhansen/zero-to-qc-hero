# 3.7.1 Quantum bomb tester
Throwback to [[049 - Unitary Transformations and the Elitzur--Vaidman Bomb]]
1. a) output port 2
b) because the bomb has to measure to see if the photon is there, isn't it a 50-50 probability at detectors 1 and 2 assuming the bomb didn't go off?
c) see [[049 - Unitary Transformations and the Elitzur--Vaidman Bomb]]

