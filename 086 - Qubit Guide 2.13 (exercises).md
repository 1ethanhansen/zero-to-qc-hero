2.13.1 [[083 - Qubit Guide 2.1 – 2.4#^159ec1|Unknown Phase]]
- What can we say about phi? only that phi was not 0. because we measured 1 as the output, there had to be some probability there and if phi was 0 the amplitutde on 1 would be 0
- Yes you can. If phi is 0, the resulting output will 0 every time, if phi is tau/2, the resulting output will be 1 every time

2.13.2 One of the many cross-product identities
This one confuses me based on what we're adding and multiplying.
$$ (\vec{a} \cdot \vec{\sigma}) \to \text{dot product of two vectors is a scalar} $$
$$ (\vec{b} \cdot \vec{\sigma}) \to \text{dot product of two vectors is a scalar} $$
$$ (\vec{a} \cdot \vec{\sigma})(\vec{b} \cdot \vec{\sigma}) \to \text{scalar times scalar is still scalar} $$
Now for the right side:
$$ (\vec{a} \cdot \vec{b}) \to \text{dot product of two vectors is a scalar} $$
$$ (\vec{a} \times \vec{b}) \to \text{cross product of two vectors is a vector} $$
$$ i(\vec{a} \times \vec{b}) \cdot \vec{\sigma} \to \text{dot product of two vectors is a scalar} $$
$$ (\vec{a} \cdot \vec{b})\mathbb{1} \to \text{scalar multiplied by a matrix is a matrix!} $$
so on the right we're trying to add a scalar to a matrix which makes 0 sense